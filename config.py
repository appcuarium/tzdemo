class BaseConfig(object):
    SECRET_KEY = '0x11\0x77\0x1\0x5c\0x8e\0x79\0x7\0x54\0x52\0x4b\0xb7\0xd7\0xad\0x55\0x24\0xf5\0x6b\0xe5\0x35\0xe2\0x72\0x82\0x75\0xe2'
    #SQLALCHEMY_DATABASE_URI = 'postgresql://localhost/bla'
    #BCRYPT_LEVEL = 12
    #MAIL_FROM_EMAIL = "sorin@appcuarium.com"


class DevelopmentConfig(BaseConfig):
    DEBUG = True


class ProductionConfig(BaseConfig):
    DEBUG = False