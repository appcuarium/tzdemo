from os import environ
from flask import Flask

# WARNING: Add credentials dictionary KEYS to your environment if you
credentials = {}

# Now store the credentials inside a dictionary
credentials['CLIENT_ID'] = environ['CLIENT_ID']
credentials['CLIENT_SECRET'] = environ['CLIENT_SECRET']
credentials['REDIRECT_URI'] = environ['REDIRECT_URI']
credentials['SCOPE'] = 'https://www.googleapis.com/auth/analytics.readonly'
credentials['ACCESS_TYPE'] = 'offline'

application = Flask(__name__)
application.config.from_object(environ['APP_SETTINGS'])

from ga_manager.views import ga_auth_blueprint

application.register_blueprint(ga_auth_blueprint)