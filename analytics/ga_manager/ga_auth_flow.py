import httplib2
from apiclient.discovery import build
from oauth2client.client import OAuth2WebServerFlow
import operator


class AuthFlow():

    # The flow object
    FLOW = None

    # The service object
    service = None

    # Store the access token
    access_token = None

    """
    Step 1. Initialize the AuthFlow object
    """
    def __init__(self, credentials):

        self.FLOW = OAuth2WebServerFlow(
            client_id=credentials['CLIENT_ID'],
            client_secret=credentials['CLIENT_SECRET'],
            scope=credentials['SCOPE'],
            redirect_uri=credentials['REDIRECT_URI'],
            access_type=credentials['ACCESS_TYPE'])

    """
    Step 2. Get the authorization url
    """
    def get_auth_url(self):

        return self.FLOW.step1_get_authorize_url()

    """
    Step 3. Exchange the code and build the Analytics object
    """
    def exchange_code(self, code):

        # Store the user credentials in a variable after exchanging the code received from Google
        credentials = self.FLOW.step2_exchange(code)

        # Authorize the user
        http = credentials.authorize(httplib2.Http())

        # Store the access token
        self.access_token = vars(credentials)['access_token']

        # Build the analytics object and store it in a static variable so it can be accessed from outside
        self.service = build('analytics', 'v3', http=http)

        # Return the service to caller
        return self.service

    """
    Step 4. Get user accounts list
    """
    def get_accounts(self):

        # Use the service object to get a list of user accounts
        accounts = self.service.management().accounts().list().execute()

        # Get accounts object from Google
        accounts_list = accounts.get('items')

        # Holds the accounts object
        accounts_data = {}

        # Initialize an empty dictionary for the response to return
        response = {}

        # If any account is received
        if accounts_list:

            # Iterate on the accounts object and extract only the necessary data
            for account in accounts_list:

                # Push the data in format { account_name : account_id }
                accounts_data[account['name']] = int(account['id'])

        # Sort the accounts by name
        sorted_accounts = sorted(accounts_data.iteritems(), key=operator.itemgetter(0))

        # Get the first account ID so it can be used to call its associated Google Analytics properties
        first_account_id = int(sorted_accounts[0][1])

        # Get properties for the first account
        properties = self.get_account_properties(first_account_id)

        if properties:

            # Store the first property ID
            first_property_id = properties[0]['id']

            # Get profiles for the first property
            profiles = self.get_account_profiles(first_account_id, first_property_id)

            # Push data to correspondent dictionary keys in the main response dictionary
            response['accounts'] = accounts_data
            response['properties'] = properties
            response['profiles'] = profiles
            response['access_token'] = self.access_token

        # Return the custom account object, or empty object if no account found
        return response

    """
    Step 5. Get connected data for a single account
    """
    def get_account_data(self, account_id):

        response = {}
        properties = self.get_account_properties(account_id)

        if len(properties) > 0:

            profiles = self.get_account_profiles(account_id, properties[0]['id'])

            response['properties'] = properties
            response['profiles'] = profiles

        return response

    """
    Step 5.1. Get account properties
    """
    def get_account_properties(self, account_id):

        response = {}

        properties = self.service.management().webproperties().list(accountId=account_id).execute()
        properties_list = properties.get('items')

        if properties_list:
            response = properties.get('items')

        return response

    """
    Step 5.2. Get account profiles(views)
    """
    def get_account_profiles(self, account_id, property_id):

        # Get a list of all Views (Profiles) for the first Web Property of the first Account
        profiles = self.service.management().profiles().list(
            accountId=account_id,
            webPropertyId=property_id).execute()

        if profiles.get('items'):
            return profiles.get('items')

    def get_service(self):
        return self.service