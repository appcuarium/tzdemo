from flask import views, render_template, Blueprint, request, jsonify
from ga_auth_flow import AuthFlow
from analytics import credentials

google_auth = AuthFlow(credentials)

ga_auth_blueprint = Blueprint(
    'ga_manager', __name__,
    template_folder='templates'
)


class Home(views.MethodView):
    @staticmethod
    def get():
        auth_url = google_auth.get_auth_url()

        return render_template('index.html', auth_url=auth_url)


ga_auth_blueprint.add_url_rule('/', view_func=Home.as_view('main'))


@ga_auth_blueprint.route('/oauth2callback')
def oauth2callback():
    # Get the authorization code from Google
    code = request.args.get('code', '')

    # Check if the response is an error
    error = request.args.get('error', '')

    if code:
        # Build the Analytics object
        service = google_auth.exchange_code(code)

        if service:
            ga_service = service

            # Get the logged in user accounts data in JSON format
            accounts = google_auth.get_accounts()

            # Render the callback page and pass the accounts data to ti
            return render_template('authorize.html', accounts=accounts)

    elif error:
        # Render the callback page and pass the accounts data to ti
        return render_template('authorize.html', error=error)


